#include <cassert>
#include <cstdlib>
#include <iostream>
#include <dlfcn.h>
#include "base.h"

struct mport : public base
{
#ifdef ACCEPT_EXTERN
	void accept(struct visitor& ) override;
#else
	void accept(struct visitor& ) override {}
#endif
	void test() override { std::cout << "host:test" << std::endl; }
};

#ifdef ACCEPT_EXTERN
void mport::accept(struct visitor& ) {}
#endif

int main(int argc, char** argv)
{
	assert(argc > 1);
	const char* library_name = argv[1];

	loader_t loader;
	void* lib = dlopen(library_name, RTLD_LAZY | RTLD_LOCAL);
	assert(lib);
	*(void **) (&loader) = dlsym(lib, "get_plugin");
	assert(loader);

	plugin* plugin = (*loader)();
	base& host_ref = plugin->port();
	host_ref.test(); // expected output: "host:test"
	plugin->test(); // expected output: "plugin:test"

	return EXIT_SUCCESS;
}

