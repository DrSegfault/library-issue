#include <iostream>
#include "base.h"

struct mport : public base
{
	void accept(struct visitor& ) override {}
	void test() override { std::cout << "plugin:test" << std::endl; }
	virtual ~mport() = default;
};

struct port_failure_plugin : public plugin
{
	void test() override final { inp.test(); }
	virtual ~port_failure_plugin() {}
private:
	mport inp;
	base& port() override { return inp; }
};

extern "C" {
const plugin* get_plugin() { return new port_failure_plugin; }
}

