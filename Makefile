FLAGS=-std=c++11 -Wall -Wextra -Werror

extern:
	g++ ${FLAGS} -DACCEPT_EXTERN -shared -fPIC plugin.cpp -o libplugin.so
	g++ ${FLAGS} -DACCEPT_EXTERN -ldl -rdynamic host.cpp -o host
intern:
	g++ ${FLAGS} -shared -fPIC plugin.cpp -o libplugin.so
	g++ ${FLAGS} -ldl -rdynamic host.cpp -o host
clean:
	rm libplugin.so host
run:
	./host $$PWD/libplugin.so
help:
	@echo "Targets: extern intern run clean"
