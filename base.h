struct base
{
	virtual void accept(struct visitor& ) {}
	virtual void test() = 0;
	virtual ~base() {}
};

struct visitor
{
	virtual void visit(struct base& ) {}
};

struct plugin
{
	virtual ~plugin() {}
	virtual base& port() = 0;
	virtual void test() = 0;
};

typedef plugin* (*loader_t) (void);

